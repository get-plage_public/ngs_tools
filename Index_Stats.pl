#! /usr/bin/perl
# $Id$

=pod

=head1 NAME

 Index_Stats.pl

=head1 SYNOPSIS

		 Injdex_Stats.pl INDEX_PATH > OUTPUT_FILE.txt
  
=head1 DESCRIPTION

 This program can generate statistics information about an index list.
 We can have the statistics of bases by position , the number of mismatches between every indexs (lower two mismatches) and error messages if the percentage of base position is too far from normal.
 

=head1 ARGUMENTS

 1-    File path. It contains the list of indexes.

 
=head1 OPTIONS

 None.
 
=head1 AUTHOR

 gaelle.vilchez@toulouse.inra.fr
 get-plage.bioinfo@genotoul.fr

=head1 VERSION

 1.2

=head1 KEYWORDS

 None.

=head1 DEPENDENCIES

 None.

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long;
use POSIX;
use List::Util;
use Data::Dumper qw(Dumper);
use List::Util qw(sum);
use Pod::Usage;



#############################################################################################################################
#
#		MAIN
#
#############################################################################################################################
MAIN:
{
	
	my $nbr_lane = '';
	my $help = 0;
	my $nbr_ech = '';
	
	GetOptions( 
			   "help" => \$help
			   );
  if( $help )
    {
             pod2usage(
                       -sections => "SYNOPSIS|ARGUMENTS|DESCRIPTION|VERSION",
                       -verbose => 99
            ) ;
      }
        
	my @list_ref = get_list_from_file( $ARGV[0] );

	print "------------------------------------------------------\n";
	print "          Fichier d'entr : $ARGV[0] \n";
	print "------------------------------------------------------\n";
	print "Valeur Statistiques sur la liste de ".scalar(@list_ref)." indexs \n";
	my @list_comp = @list_ref ;	
	
#########
	my $compt = 0;
#########################################################################################################################################
	print "------------------------------------------------------\n";
	

#########	
    List_index(@list_ref);
	my @position = calcul(@list_ref);	
	#####################################
	my $longu = scalar(@list_ref);
	my $lenIndex = length($list_ref[0]);
	for ( my $i = 0 ; $i < $lenIndex; $i++ ) {
		print "		---		\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][0])*100/$longu)."% de A\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][1])*100/$longu)."% de C\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][2])*100/$longu)."% de T\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][3])*100/$longu)."% de G\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][0]+$position[$i][1])*100/$longu)."% de A-C";
				if (int(($position[$i][0]+$position[$i][1])*100/$longu) < 40 or int(($position[$i][0]+$position[$i][1])*100/$longu) > 60) {
					print "   -> Attention le pourcentage de ces bases s'eloignent trop de 50% !";
				}
				print "\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][2]+$position[$i][3])*100/$longu)."% de T-G";
				if (int(($position[$i][0]+$position[$i][1])*100/$longu) < 40 or int(($position[$i][0]+$position[$i][1])*100/$longu) > 60) {
					print "   -> Attention le pourcentage de ces bases s'eloignent trop de 50% !";
				} 
				print "\n";
	}
	print "		---		\n";
		
   		 	$compt++;

	}

#############################################################################################################################
#
#		SUB
#
#############################################################################################################################


=head2 function calcul 

 Title        : calcul 
 Usage        : calcul (@liste)
 Prerequisite : none
 Function     : Retourne un tableau contenant le comptage des bases par position.
 Returns      : integer
 Args         : @liste. Une liste 
 Globals      : none

=cut
sub calcul {
	my (@list) = @_;
	my @ref_list  = split( //,@list ) ;
	my %position = ();
	my $nbrA =1; my $nbrC =1;
	my $nbrT =1; my $nbrG =1;
	my @base = ();
	my $long = scalar(@list);
	my @position = ();
	my $len = 0;
	for( my $i = 0 ; $i < $long ; $i++ ) {
		my $index = $list[$i];
		my $len = length($index);
		for (my $j = 0 ; $j < $len+1 ; $j++ ) { 
			for (my $b = 0 ; $b < 4 ; $b ++){
				$position[$j][$b] = 0;
			}
		}
	}
	
	for( my $i = 0 ; $i < $long ; $i++ ) {

		foreach my $index ($list[$i]) {
		
			#print "------------------------------- \n Index ".($i+1)."\n-------------------------------\n";

			my @stra = ();	my @strc = ();
			my @strt = ();	my @strg = ();
			my @uniqueA = (); my @uniqueC = ();
			my @uniqueT = (); my @uniqueG = ();
			my %dejavuA = (); my %dejavuC = ();
			my %dejavuT = (); my %dejavuG = ();
			my $offseta = 0; my $offsetc = 0;
			my $offsett = 0; my $offsetg = 0;
			my @base1 = (); my @base2 = ();
			my @base3 = (); my @base4 = ();
			my @base5 = (); my @base6 = ();
			my $len = length($index);

			
			my $ra = index ($index, "A",$offseta);	####A
			while ($ra != -1) {
				push @stra, $ra;
				$offseta = $ra + 1;
				$ra = index ($index, "A",$offseta);
			} 
			foreach my $elem ( @stra ){
			next if $dejavuA{ $elem }++;
			push @uniqueA, $elem;
			}
			
			my $rc = index ($index, "C",$offsetc);	####C
			while ($rc != -1) {
				push @strc, $rc;
				$offsetc = $rc + 1;
				$rc = index ($index, "C",$offsetc);
			}
			foreach my $elem ( @strc ){
			next if $dejavuC{ $elem }++;
			push @uniqueC, $elem;
			}

			my $rt = index ($index, "T",$offsett);	####T
			while ($rt != -1) {
				push @strt, $rt;
				$offsett = $rt + 1;
				$rt = index ($index, "T",$offsett);
			}
			foreach my $elem ( @strt ){
			next if $dejavuT{ $elem }++;
			push @uniqueT, $elem;
			}
			
			my $rg = index ($index, "G",$offsetg);	####G
			while ($rg != -1) {
				push @strg, $rg;
				$offsetg = $rg + 1;
				$rg = index ($index, "G",$offsetg);
			} 
			foreach my $elem ( @strg ){
			next if $dejavuG{ $elem }++;
			push @uniqueG, $elem;
			}
			
			
			for (my $j = 0 ; $j < $len+1 ; $j++ ) { 

				foreach my $a (@uniqueA) {
					if ($a == $j) {
					$base[0] = $nbrA++;
					$position[$j][0]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un A\n";
					}
				}
				foreach my $c (@uniqueC) {
					if ($c == $j) {
					$base[1] = $nbrC++;
					$position[$j][1]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un C\n";
					}
				}
				foreach my $t (@uniqueT) {
					if ($t == $j) {
					$base[2] = $nbrT++;
					$position[$j][2]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un T\n";
					}
				}
				foreach my $g (@uniqueG) {
					if ($g == $j) {
					$base[3] = $nbrG++;
					$position[$j][3]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un G\n";
					}
				}

			}		
		}
	}
	return @position;
} 

=head2 function List_index 

 Title        : List_index 
 Usage        : List_index (@list_ref)
 Prerequisite : none
 Function     : Retourne une liste avec deux index et leurs scores.
 Returns      : integer
 Args         : @list_ref. Liste d'index
 Globals      : none

=cut
sub List_index {

	my @list_ref = @_;
	my @list_comp = @list_ref ;
	my @ref_list  = split( //,@list_ref ) ;
	my @comp_list = split( //,@list_comp ) ;
	my %hashtab = ();
	my %hashtab_simple = ();
	my @couplindex = ();
	
	
	foreach my $cur_ref (@list_ref) {
		shift (@list_comp) ;
		foreach my $cur_comp (@list_comp) {
			for ( my $i = 0 ; $i < scalar(@list_ref)/340 ; $i++ ){
				if (count_diff($cur_ref, $cur_comp) == 1){
					print "\nATTENTION il n'y a qu'un seul mismatch entre les indexs ".$cur_ref." et ".$cur_comp."\n";					
				}
				if (count_diff($cur_ref, $cur_comp) == 2){
					#print "\nATTENTION il n'y a que deux mismatchs entre les indexs ".$cur_ref." et ".$cur_comp."\n";					
				}
								if (count_diff($cur_ref, $cur_comp) == 3){
					#print "\nATTENTION il n'y a que trois mismatchs entre les indexs ".$cur_ref." et ".$cur_comp."\n";					
				}
				if (count_diff($cur_ref, $cur_comp) == 0){
					print "\nATTENTION Il y a un doublons ".$cur_ref." et ".$cur_comp."\n";					
				}
			}
		}
	}
}

=head2 function get_list_from_file 

 Title        : get_list_from_file 
 Usage        : get_list_from_file ($file_path)
 Prerequisite : none
 Function     : Retourne une liste a partir d'un fichier.
 Returns      : liste
 Args         : $file_path  String - Chemin du ficher.
 Globals      : none

=cut
sub get_list_from_file {
	my( $file_path ) = @_ ;
	my @lines ;
	
	open( my $FH_IN, $file_path) or die "Can't open ".$file_path." : ".$!."\n";
	while ( my $cur_line = <$FH_IN> ) {
		chomp $cur_line ;
		push( @lines, $cur_line );
	}
	
	close $FH_IN ;

	return @lines ;
}

=head2 function count_diff 

 Title        : count_diff 
 Usage        : count_diff ($ref, $comp )
 Prerequisite : none
 Function     : Retourne le nombre de diffrences entre deux listes d'index.
 Returns      : integer
 Args         : $ref, $comp  String - Deux indexs.
 Globals      : none

=cut
sub count_diff {
	my( $ref, $comp ) = @_ ;

	my $nb_diff = 0 ;

	my @ref_fields  = split( //, $ref ) ;
	my @comp_fields = split( //, $comp ) ;

	for( my $i = 0 ; $i < scalar(@ref_fields) ; $i++ ) {
		if( $ref_fields[$i] ne $comp_fields[$i] ) {
			$nb_diff++ ;
		}
	}
	
	return $nb_diff ;
}
