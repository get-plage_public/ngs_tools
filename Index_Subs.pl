#! /usr/bin/perl
# $Id$

=pod

=head1 NAME

 Index_Subs

=head1 SYNOPSIS

		 Index_Subs.pl INDEX_PATH File_conf > OUTPUT_FILE.txt
  
=head1 DESCRIPTION

 This program can generate a sub index list from another index list.
 It will be took into accont the number of mismatchs between each indexes, 
 bases ratios for the Illumina lazer, 
 and finally the percentage of bases in each position.

=head1 ARGUMENTS

 1-    File path. It contains the list of indexes.
 2-	   File_conf : The configuration file contains the number of sub-list to be generated. It is in the form:
 		Lane1	nbr_of_index
 		Lane2	nbr_of_index
 		Lane3	...
 
=head1 OPTIONS

 None.
 
=head1 AUTHOR

 get-plage.bioinfo@genotoul.fr

=head1 VERSION

 1.2

=head1 KEYWORDS

 None.

=head1 DEPENDENCIES

 None.

=cut


#############################################################################################################################
#
#		LIBRAIRIES
#
#############################################################################################################################
use strict ;
use warnings ;
use Getopt::Long;
use POSIX;
use List::Util;
use Data::Dumper qw(Dumper);
use List::Util qw(sum);
use Pod::Usage;



#############################################################################################################################
#
#		MAIN
#
#############################################################################################################################
MAIN:
{
	
	my $nbr_lane = '';
	my $help = 0;
	my $nbr_ech = '';
	
	GetOptions(
			   'NbrEchantillon=i' => \$nbr_ech, 
			   'NbrLane=i' => \$nbr_lane,
			   "help" => \$help
			   );

	 if( $help )
      {
              pod2usage(
                        -sections => "SYNOPSIS|ARGUMENTS|DESCRIPTION|VERSION",
                        -verbose => 99
           ) ;        
}


	my @list_ref = get_list_from_file( $ARGV[0] );
	my @liste_conf = get_conf_from_file( $ARGV[1] );
	my $nb_lane = scalar(@liste_conf);
	print "Le but est de faire $nb_lane liste(s) d'indexs\n";
	foreach my $b (@liste_conf){
		chomp $b;
		print "Une liste de $b\n";
	}

	my @list_comp = @list_ref ;	
	my %listeIndex = List_index(get_list_from_file( $ARGV[0] ));
	my @liste = tri_scoring(%listeIndex);
	my $compt = 0;
	while ($compt != $nb_lane){ 
	print "------------------------------------------------------\n";
	print "Liste N ".int($compt+1)." - Nombre d'index souhait : $liste_conf[$compt] \n"; 
	print "------------------------------------------------------\n";

	my @ListeBest = ();
	my $nbr_pourc = 2;		
	@ListeBest = coupe($liste_conf[$compt], $nbr_pourc, @liste);


	if ((scalar(@ListeBest) != $liste_conf[$compt]) and (scalar(@liste) > $liste_conf[$compt] )){
			while (scalar(@ListeBest) != $liste_conf[$compt]){
				@ListeBest = coupe($liste_conf[$compt], $nbr_pourc, @liste);
				$nbr_pourc++;
			}
	}
	
	my $nbrlistebest = scalar(@ListeBest);
	my $nbrssliste = $liste_conf[$compt];
	my $nbrliste = scalar(@liste);
	
	if (scalar(@liste) < $liste_conf[$compt] ) {
		print "\nLa liste de rfrence ne contient pas assez d'indexs pour gnrer une nouvelle sous-liste de longueur $nbrssliste\n ";
		exit;
	}
	
	if (scalar(@ListeBest) == $liste_conf[$compt]) {
		print "\nListe gnre avec un pourcentage d'erreur autoris de $nbr_pourc% \n";
		print "\nLa longueur de la liste est de ".$nbrlistebest." :\n\n";
		foreach my $w (@ListeBest){
			print "$w\n";
		}
	}
	print "------------------------------------------------------\n";
	
	my @stat = calcul(@liste);
	my @position = calcul(@ListeBest);
	print "------------------------------- \n	Donnees Statistiques par position dans notre sous liste de $nbrlistebest  \n";
	my $longu = scalar(@ListeBest);
	my $indlen = length($ListeBest[1]);
	for ( my $i = 0 ; $i < $indlen-1; $i++ ) {
		print "		---		\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][0])*100/$longu)."% de A\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][1])*100/$longu)."% de C\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][2])*100/$longu)."% de T\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][3])*100/$longu)."% de G\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][0]+$position[$i][1])*100/$longu)."% de A-C\n";
				print "Position ".($i+1)." nous avons ".int(($position[$i][2]+$position[$i][3])*100/$longu)."% de T-G\n"; 
	}
	print "		---		\n";
		
	for (my $val = 0 ; $val < scalar(@ListeBest) ; $val++ ){
		for (my $val_ref = 0 ; $val_ref < scalar(@liste) ; $val_ref++){
			if ($ListeBest[$val] eq $liste[$val_ref]){
      		@liste = Delete_Tab($val_ref, @liste);
      		}
		}
	}
   		 	$compt++;
  		}

	}

#############################################################################################################################
#
#		SUB
#
#############################################################################################################################


=head2 function calcul 

 Title        : calcul 
 Usage        : calcul (@liste)
 Prerequisite : none
 Function     : Retourne un tableau contenant le comptage des bases par position.
 Returns      : integer
 Args         : @liste. Une liste 
 Globals      : none

=cut
sub calcul {
	my (@list) = @_;
	my @ref_list  = split( //,@list ) ;
	my %position = ();
	my $nbrA =1; my $nbrC =1;
	my $nbrT =1; my $nbrG =1;
	my @base = ();
	my $long = scalar(@list);
	my @position = ();
	my $len = 0;
	for( my $i = 0 ; $i < $long ; $i++ ) {
		my $index = $list[$i];
		my $len = length($index);
		for (my $j = 0 ; $j < $len+1 ; $j++ ) { 
			for (my $b = 0 ; $b < 4 ; $b ++){
				$position[$j][$b] = 0;
			}
		}
	}
	
	for( my $i = 0 ; $i < $long ; $i++ ) {

		foreach my $index ($list[$i]) {
		
			#print "------------------------------- \n Index ".($i+1)."\n-------------------------------\n";

			my @stra = ();	my @strc = ();
			my @strt = ();	my @strg = ();
			my @uniqueA = (); my @uniqueC = ();
			my @uniqueT = (); my @uniqueG = ();
			my %dejavuA = (); my %dejavuC = ();
			my %dejavuT = (); my %dejavuG = ();
			my $offseta = 0; my $offsetc = 0;
			my $offsett = 0; my $offsetg = 0;
			my @base1 = (); my @base2 = ();
			my @base3 = (); my @base4 = ();
			my @base5 = (); my @base6 = ();
			my $len = length($index);

			
			my $ra = index ($index, "A",$offseta);	####A
			while ($ra != -1) {
				push @stra, $ra;
				$offseta = $ra + 1;
				$ra = index ($index, "A",$offseta);
			} 
			foreach my $elem ( @stra ){
			next if $dejavuA{ $elem }++;
			push @uniqueA, $elem;
			}
			
			my $rc = index ($index, "C",$offsetc);	####C
			while ($rc != -1) {
				push @strc, $rc;
				$offsetc = $rc + 1;
				$rc = index ($index, "C",$offsetc);
			}
			foreach my $elem ( @strc ){
			next if $dejavuC{ $elem }++;
			push @uniqueC, $elem;
			}

			my $rt = index ($index, "T",$offsett);	####T
			while ($rt != -1) {
				push @strt, $rt;
				$offsett = $rt + 1;
				$rt = index ($index, "T",$offsett);
			}
			foreach my $elem ( @strt ){
			next if $dejavuT{ $elem }++;
			push @uniqueT, $elem;
			}
			
			my $rg = index ($index, "G",$offsetg);	####G
			while ($rg != -1) {
				push @strg, $rg;
				$offsetg = $rg + 1;
				$rg = index ($index, "G",$offsetg);
			} 
			foreach my $elem ( @strg ){
			next if $dejavuG{ $elem }++;
			push @uniqueG, $elem;
			}
			
			
			for (my $j = 0 ; $j < $len+1 ; $j++ ) { 

				foreach my $a (@uniqueA) {
					if ($a == $j) {
					$base[0] = $nbrA++;
					$position[$j][0]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un A\n";
					}
				}
				foreach my $c (@uniqueC) {
					if ($c == $j) {
					$base[1] = $nbrC++;
					$position[$j][1]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un C\n";
					}
				}
				foreach my $t (@uniqueT) {
					if ($t == $j) {
					$base[2] = $nbrT++;
					$position[$j][2]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un T\n";
					}
				}
				foreach my $g (@uniqueG) {
					if ($g == $j) {
					$base[3] = $nbrG++;
					$position[$j][3]++;
					#print "Dans l'index ".($i+1)." a la position $j il y a un G\n";
					}
				}

			}		
		}
	}
	return @position;
}
	


=head2 function suite 

 Title        : suite 
 Usage        : suite (@liste)
 Prerequisite : none
 Function     : Retourne une suite des valeurs limites pour le calcul des pourcentage de diffrence tolr entre chaque index.
 Returns      : integer
 Args         : @liste. Une liste 
 Globals      : none

=cut
sub suite {
	my (@liste) = @_;
	my $longliste = @liste;
	my @suite = ();
	my $n = 1;
	my $x = 0;
	
	for (my $i =2 ; $i < $longliste ; $i++){     #le numero de l'index
			if ($i%4 == 1){
				$n++;
			}
				$x = $n*100/$i;
				$suite[$i] = $x;
				#print "valeur de i%4 : ".($i%4)." valeur de n : $n ---- valeur de i : $i valeur de x : $x\n";
		}

	return @suite;
}

=head2 function reverse_complement 

 Title        : reverse_complement 
 Usage        : reverse_complement ($index)
 Prerequisite : none
 Function     : Retourne le reverse complement de l'index donn.
 Returns      : integer
 Args         : $dna. chaine de caractre 
 Globals      : none

=cut
sub reverse_complement {
        my $dna = shift;
        my $revcomp = reverse($dna);
        $revcomp =~ tr/ACGTacgt/TGCAtgca/;
        #print $revcomp;
        return $revcomp;
}


=head2 function coupe 

 Title        : coupe 
 Usage        : coupe ($nbr_ech, $nbr_pourc, $liste_ref, $liste2_ref)
 Prerequisite : none
 Function     : Retourne une liste avec le nombre d'index souhait.
 Returns      : integer
 Args         : $nbr_ech, $nbr_pourc, $liste_ref, $liste2_ref. Liste d'index
 Globals      : none

=cut
sub coupe {
	my ($nbr_ech, $nbr_pourc, @liste) = @_;
	my @newliste = ();
	my $longliste = @liste;
	my @position = ();	
	my $longnewliste = 0;
	my $var = 0;
	my @suite = suite(@liste);
	my @liste_reverse =();
		my $indlen = length($liste[1]);

	foreach my $h (@liste){
		push (@liste_reverse, reverse_complement($h));
	}
	my %liste_reverse = map {$_ => 1} @liste_reverse;

	@newliste = @liste[0..2]; # longueur de la newliste est de 2
		foreach my $v (@newliste){
			$longnewliste++;
			}	
		foreach my $r (@liste){
			if ($longnewliste == 2){
				push (@newliste , $r); 
				my @position = calcul(@newliste);
				$longnewliste = @newliste;
				for ( my $i = 0 ; $i < 5; $i++ ) { #les 3 premiers
					if ($position[$i][0] > 1 or
						$position[$i][1] > 1 or
						$position[$i][2] > 1 or
						$position[$i][3] > 1) {
							my $u = pop (@newliste);
							$longnewliste = @newliste;
							last;	
					}		
				}
			}
		}
				foreach my $r (@liste){				#les suivants
					if ($longnewliste > 2){
						
						my %newliste = map {$_ => 1} @newliste;
						if ($longnewliste == $nbr_ech){last;}
						if (( !defined $newliste{$r}) and ( !defined $liste_reverse{$r}))  {
							push (@newliste ,$r);
							my @position = calcul(@newliste);	
							$longnewliste = @newliste;
							#print "$longnewliste \n";
							#print "valeur de la suite : ".int($suite[$longnewliste])." avec comme nombre d'index dans la nouvelle liste $longnewliste\n";
							for ( my $j = 0 ; $j < $indlen-1; $j++ ) {
								if ((int(($position[$j][0])*100/$longnewliste) > (int($suite[$longnewliste])+$nbr_pourc) or	
									int(($position[$j][0])*100/$longnewliste) < (int($suite[$longnewliste])-$nbr_pourc)) and								
									(int(($position[$j][1])*100/$longnewliste) > (int($suite[$longnewliste])+$nbr_pourc) or
									int(($position[$j][1])*100/$longnewliste) < (int($suite[$longnewliste])-$nbr_pourc)) and								
									(int(($position[$j][2])*100/$longnewliste) > (int($suite[$longnewliste])+$nbr_pourc) or
									int(($position[$j][2])*100/$longnewliste) < (int($suite[$longnewliste])-$nbr_pourc)) and
									(int(($position[$j][3])*100/$longnewliste) > (int($suite[$longnewliste])+$nbr_pourc) or							
									int(($position[$j][3])*100/$longnewliste) < (int($suite[$longnewliste])-$nbr_pourc) )) {
										my $t = pop (@newliste);
										$longnewliste = @newliste;
										last;
								}								
							}			
						}		
					}
			}	

	return @newliste;
}

=head2 function tri_scoring 

 Title        : tri_scoring 
 Usage        : tri_scoring (%hash)
 Prerequisite : none
 Function     : Retourne une liste avec des indexs tris selon leurs scrores.
 Returns      : integer
 Args         : %hash. Liste d'index
 Globals      : none

=cut
sub tri_scoring {
	
	my (%hash) = @_ ;
	my %newtab = ();
	my %tri = ();
	my $nb = keys(%newtab);
	my @best = ();
	
	
	foreach my $k ( reverse sort({ $hash{$a}[0] <=> $hash{$b}[0] } keys %hash)) {
		#print "$k\t$hash{$k}[0]\n";
		push (@best, split(' \| ', $k));	
	}
	
	my $compt = @best;
	my @unique = ();
    my %dejavu = ();
	foreach my $elem ( @best ){
      next if $dejavu{ $elem }++;
      push @unique, $elem;
    }
    print "\n------------------------------------------------------\n\n";
    my $uni = @unique;
	print "Nombre d'index unique dans la liste de rfrence : $uni \n";
	foreach my $c (@unique){
		print "$c\n";
	}
    print "\n------------------------------------------------------\n\n";
    return @unique;
} 

=head2 function List_index 

 Title        : List_index 
 Usage        : List_index (@list_ref)
 Prerequisite : none
 Function     : Retourne une liste avec deux index et leurs scores.
 Returns      : integer
 Args         : @list_ref. Liste d'index
 Globals      : none

=cut

sub List_index {

	my @list_ref = @_;
	my @list_comp = @list_ref ;
	my @ref_list  = split( //,@list_ref ) ;
	my @comp_list = split( //,@list_comp ) ;
	my %hashtab = ();
	my %hashtab_simple = ();
	my @couplindex = ();
	
	
	foreach my $cur_ref (@list_ref) {
		shift (@list_comp) ;
		foreach my $cur_comp (@list_comp) {
			for ( my $i = 0 ; $i < scalar(@list_ref)/340 ; $i++ ){
				my $scoreglob = int(cont_lazer($cur_ref, $cur_comp))+int(count_diff($cur_ref, $cur_comp));
				if (count_diff($cur_ref, $cur_comp) == 1){
					#print "\nATTENTION il n'y a qu'un seul mismatch entre les indexs ".$cur_ref." et ".$cur_comp."\n";					
				}
				if (count_diff($cur_ref, $cur_comp) == 2){
					print "\nATTENTION il n'y a que deux mismatchs entre les indexs ".$cur_ref." et ".$cur_comp."\n";					
				}
								if (count_diff($cur_ref, $cur_comp) == 3){
				#	print "\nATTENTION il n'y a que trois mismatchs entre les indexs ".$cur_ref." et ".$cur_comp."\n";					
				}
				if (count_diff($cur_ref, $cur_comp) == 0){
					print "\nATTENTION Il y a un doublons ".$cur_ref." et ".$cur_comp."\n";					
				}
				#print "diff : \t".count_diff($cur_ref, $cur_comp)."\t lazer : \t".cont_lazer($cur_ref, $cur_comp)."\t global : \t".$scoreglob."\t".$cur_ref."\t".$cur_comp."\n" ;
				@couplindex=($cur_ref,$cur_comp);
				push @{ $hashtab {"$cur_ref | $cur_comp"} } , $scoreglob;
				push @{ $hashtab_simple{"$cur_ref"}}, $scoreglob;
				push @{ $hashtab_simple{"$cur_comp"}}, $scoreglob;
			}
		}
	}
	return %hashtab ;
}


=head2 function count_diff 

 Title        : count_diff 
 Usage        : count_diff ($ref, $comp )
 Prerequisite : none
 Function     : Retourne le nombre de diffrences entre deux listes d'index.
 Returns      : integer
 Args         : $ref, $comp  String - Deux indexs.
 Globals      : none

=cut
sub count_diff {
	my( $ref, $comp ) = @_ ;

	my $nb_diff = 0 ;

	my @ref_fields  = split( //, $ref ) ;
	my @comp_fields = split( //, $comp ) ;

	for( my $i = 0 ; $i < scalar(@ref_fields) ; $i++ ) {
		if( $ref_fields[$i] ne $comp_fields[$i] ) {
			$nb_diff++ ;
		}
	}
	
	return $nb_diff ;
}


=head2 function cont_lazer 

 Title        : cont_lazer 
 Usage        : cont_lazer ($ref, $comp )
 Prerequisite : none
 Function     : Retourne un score de compatibilit entre deux index.
 Returns      : integer
 Args         : $ref, $comp  String - Deux indexs.
 Globals      : none

=cut
sub cont_lazer {
	my( $ref, $comp ) = @_ ;
	my $scorelz = 0 ;

	my @ref_fields  = split( //, $ref ) ;
	my @comp_fields = split( //, $comp ) ;

	for( my $i = 0 ; $i < scalar(@ref_fields) ; $i++ ) {
		if ((( $ref_fields[$i] eq "A" ) && ( $comp_fields[$i] eq "C" )) or (( $ref_fields[$i] eq "C" ) && ( $comp_fields[$i] eq "A" ))) {
			$scorelz--;
		}
		if ((( $ref_fields[$i] eq "T" ) && ( $comp_fields[$i] eq "G" )) or (( $ref_fields[$i] eq "G" ) && ( $comp_fields[$i] eq "T" ))) {
			$scorelz--;
		}
	}
	return $scorelz ;
}

=head2 function get_list_from_file 

 Title        : get_list_from_file 
 Usage        : get_list_from_file ($file_path)
 Prerequisite : none
 Function     : Retourne une liste a partir d'un fichier.
 Returns      : liste
 Args         : $file_path  String - Chemin du ficher.
 Globals      : none

=cut
sub get_list_from_file {
	my( $file_path ) = @_ ;
	my @lines ;
	
	open( my $FH_IN, $file_path) or die "Can't open ".$file_path." : ".$!."\n";
	while ( my $cur_line = <$FH_IN> ) {
		chomp $cur_line ;
		push( @lines, $cur_line );
	}
	
	close $FH_IN ;

	return @lines ;
}


=head2 function get_conf_from_file 

 Title        : get_conf_from_file 
 Usage        : get_conf_from_file ($file_path)
 Prerequisite : none
 Function     : Retourne les configurations a partir du fichier de conf.
 Returns      : liste
 Args         : $file_path  String - Chemin du ficher de configuration.
 Globals      : none

=cut
sub get_conf_from_file {
	my( $file_path ) = @_ ;
	my @conf ;
	
	open( my $FH_IN, $file_path) or die "Can't open ".$file_path." : ".$!."\n";
	while ( my $cur_line = <$FH_IN> ) {
		chomp $cur_line ;
		my @line = split (/\t/, $cur_line);
		chomp $line[1];
		push( @conf, $line[1] );
	}
	
	close $FH_IN ;

	return @conf ;
}


=head2 function Delete_Tab

 Title        : Delete_Tab
 Usage        : Delete_Tab(@tab)
 Prerequisite : none
 Function     : Retourne les informations contenues dans la sampleSheet.
 Returns      : tableau
 Args         : $sample_sheet_path  String - Chemin de la sample sheet.
 Globals      : none

=cut
sub Delete_Tab
{
    my $Position = shift;
    my @OldTab = @_;
    my @NewTab;
 
    for (my $i = 0; $i < scalar(@OldTab); ++$i)
    {
        push(@NewTab, $OldTab[$i]) if ($i != $Position);
    }
 
    return @NewTab;
}


