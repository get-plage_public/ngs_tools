# NGS_tools
GeT-PlaGe's public repository of scripts for NGS analyses.  
Feel free to clone, contribute, and distribute.  
Please credit GeT-PlaGe when applicable.  

## Index_Stats.pl
This program can generate statistics information given a list of indexes.
Displays statistics of bases by position, the number of mismatches between every index (lower than one or two mismatches, depending on which machine is used) and error messages if the percentage of base positions is too far from normal (ideally about 25% +/- 10%). 
 
### Usage:
`Index_Stats.pl INDEX_PATH.txt > OUTPUT_FILE.txt`  
 Example of INDEX_PATH.txt:
```
GAACTGAGCG-TCGTGGAGCG
AGGTCAGATA-CTACAAGATA
CGTCTCATAT-TATAGTAGCT
...
```
Example of OUTPUT_FILE.txt
```
------------------------------------------------------
          Fichier d'entr : INDEX_PATH.txt
------------------------------------------------------
Valeur Statistiques sur la liste de 384 indexs
------------------------------------------------------
                ---
Position 1 nous avons 27% de A
Position 1 nous avons 25% de C
Position 1 nous avons 27% de T
Position 1 nous avons 19% de G
Position 1 nous avons 52% de A-C
Position 1 nous avons 47% de T-G
                ---
Position 2 nous avons 25% de A
Position 2 nous avons 23% de C
Position 2 nous avons 27% de T
Position 2 nous avons 23% de G
Position 2 nous avons 48% de A-C
Position 2 nous avons 51% de T-G
                ---
                ...
                ---
Position 21 nous avons 29% de A
Position 21 nous avons 12% de C
Position 21 nous avons 30% de T
Position 21 nous avons 27% de G
Position 21 nous avons 41% de A-C
Position 21 nous avons 58% de T-G
                ---

```

## Index_Subs.pl

### Usage:
`Index_Subs.pl INDEX_PATH File_conf > OUTPUT_FILE.txt`  
 Example of INDEX_PATH.txt:
```
GAACTGAGCG-TCGTGGAGCG
AGGTCAGATA-CTACAAGATA
CGTCTCATAT-TATAGTAGCT
...
```
Example of File_conf (number of sub-list to be generated per lane):
```
Lane1   18
Lane2   20
```
Example of OUTPUT_FILE.txt (given only Lane1 in the conf file):
```
Le but est de faire 1 liste(s) d'indexs
Une liste de 18

------------------------------------------------------

Nombre d'index unique dans la liste de référence : 384
AGTGTTGCAC-CTGGTACACG
GACACCATGT-TCAACGTGTA
 ...
 ------------------------------------------------------

------------------------------------------------------
Liste N 1 - Nombre d'index souhaité : 18
------------------------------------------------------

Liste générée avec un pourcentage d'erreur autorisé de 41%

La longueur de la liste est de 18 :

AGTGTTGCAC-CTGGTACACG
GACACCATGT-TCAACGTGTA
GAGAATGGTT-TCGGCAGCAA
 ...
------------------------------------------------------
-------------------------------
        Donnees Statistiques par position dans notre sous liste de 18
                ---
Position 1 nous avons 16% de A
Position 1 nous avons 22% de C
Position 1 nous avons 16% de T
Position 1 nous avons 44% de G
Position 1 nous avons 38% de A-C
Position 1 nous avons 61% de T-G
                ---
Position 2 nous avons 38% de A
Position 2 nous avons 11% de C
Position 2 nous avons 22% de T
Position 2 nous avons 27% de G
Position 2 nous avons 50% de A-C
Position 2 nous avons 50% de T-G
                ---
 ...
```
